/**
 * 
 */
package ca.qc.banq.idel.extranet.rest;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.qc.banq.idel.extranet.models.AuthAttribute;
import lombok.extern.slf4j.Slf4j;

/**
 * Mapping des URI de base de l'application
 * @author <a href="mailto:francis.djiomou@banq.qc.ca.com">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-17
 */
@Slf4j
@Controller
@RequestMapping("/")
public class MainController {

	@Value("${idel.session.timeout}")
	private String timeOut;

	@Autowired
	ServletContext servletContext;

	@ResponseBody
	@GetMapping(value = {"/extranet", "/extranet/*"})
	public Resource redirectHome(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		
		// Recuperation du code usager et du profil
		AuthAttribute attrs = readAuthAttributes(req);

		String token = buildToken(attrs);
		
		log.trace("Attributs recuperes de Shibboleth: " + attrs.toString());
		if(attrs.getUid() != null && !isTokenIntoRequest(req) ) {
			log.trace("Shibboleth-Auth: " + token);
			log.trace( "voici les cookies qui restent dans la reponse : " + req.getCookies());
			Cookie cookieUID = new Cookie("uid", attrs.getUid()); cookieUID.setSecure(true); cookieUID.setPath(req.getContextPath() + "/extranet"); 
			Cookie cookieToken = new Cookie("token", token); cookieToken.setSecure(true); cookieToken.setPath(req.getContextPath() + "/extranet"); cookieToken.setMaxAge(Integer.valueOf(timeOut) * 60);
			Cookie cookieProfil = new Cookie("profil", attrs.getProfileextranet()); cookieProfil.setSecure(true); cookieProfil.setPath(req.getContextPath() + "/extranet");
			resp.addCookie( cookieUID );
			resp.addCookie( cookieToken );
			resp.addCookie( cookieProfil );
		}
		return new ClassPathResource("views/extranet/index.html");

	}

	
	@ResponseBody
	@GetMapping(value = {"/public", "/public/*"} )
	public Resource redirectPublic() {
		return new ClassPathResource("views/extranet/index.html");
	}

	private AuthAttribute readAuthAttributes(HttpServletRequest req) {
		return new AuthAttribute((String)req.getAttribute("uid"), (String)req.getAttribute("sn"), (String)req.getAttribute("roles") ,(String)req.getAttribute("profileextranet"), (String)req.getAttribute("preferredlocale"), (String)req.getAttribute("pivotalid"), expirationDate(), (String)req.getAttribute("givenName"), (String)req.getAttribute("commonName"));
	}

	private String buildToken(AuthAttribute attrs) throws Exception {
		return Base64.getEncoder().encodeToString( new ObjectMapper().writeValueAsString(attrs).getBytes() );
	}
	
	private String expirationDate() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, Integer.valueOf(timeOut));
		return new SimpleDateFormat("yyyyMMddHHmmss").format( cal.getTime() );
	}
	

	private boolean isTokenIntoRequest(HttpServletRequest req) {
		boolean res = false;
		for(Cookie c : req.getCookies()) {
			if (c.getName().equalsIgnoreCase("token")) {
				try {
					AuthAttribute val = new ObjectMapper().readValue( Base64.getDecoder().decode(c.getValue()) , AuthAttribute.class);
					log.trace( "On a trouve un autre token dans l'entete. token= " + c.getValue() + ", Value = " +  val + ", age = " + c.getMaxAge());
					res = val != null && c.getMaxAge() > 0;
				} catch(Exception ex) {log.error(ex.getMessage(), ex);}
				break;
			}
		}
		return res;
	}
}
