/**
 * 
 */
package ca.qc.banq.idel.extranet.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author francis.djiomou
 *
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuthAttribute {

	private String uid;
	private String sn;
	private String roles;
	private String profileextranet;
	private String preferredlocale;
	private String pivotalid;
	/**
	 * Date d'expiration de la session (au format yyyyMMddHHmmss)
	 */
	private String expDate;
	private String givenName;
	private String commonName;
	
}
