----------------------------------------------
IDEL-EXTRANET-SERVICE
----------------------------------------------

## Intro
Le projet idel-extranet-service est une application SpringBoot qui fait office de lanceur pour la couche de présentation développée sous Angular. L'objectif de ce projet est qu'une fois l'application angular finalisée, son livrable (contenu du dist) soit encapsulé parmi les ressources du service afin qu'elle puisse être servie lors du demarrage du conteneur embarqué tomcat. Ce projet permet aussi d'encapsuler la dite application dans le contexte de sécurité de l'authentification sso avec Shibboleth (accessibilité via https).


## Presentation
Le projet est une application SpringBoot simple constituée :
   - des configurations du serveur embarqué (classes du package ca.qc.banq.idel.extranet.config + src/main/resources/config)
   - d'un controlleur Web permettant routage des URI de base de l'application (contenu dans le package ca.qc.banq.idel.extranet.rest)
   - des ressources statiques (pages web) représentant le contenu des pages web a servir (src/main/resources/views)
   - du fichier pom.xml (fichier de configuration Maven)


```
idel-extranet-service/
├── pom.xml
├── src/
│   ├── main/
│   │   ├── java/
│   │   │   ├── ca.qc.banq.idel.extranet.config/
│   │   │   └── ca.qc.banq.idel.extranet.rest/
│   │   ├── resources/
│   │   │   ├── config/
│   │   │   │   ├── application.properties
│   │   │   │   ├── application-DEV.properties
│   │   │   │   └── application-PROD.properties
│   │   │   ├── views/
│   │   │   │   ├── extranet/
│   │   │   │   ├── docs/
│   │   │   │   └── public/
│   │   │   └── application.properties
```


## Dependances
Les dépendances ici sont les différents modules nécessaires a la construction du livrable du service dans le but de le déployer sur un serveur.
   1. Java 1.8+
   2. Maven 3.6.2+
   3. Git


## Installation
L'installation ici consiste simplement a recuperer les sources du projet sur le serveur bitbucket.

``` bash
$ git clone https://djiomoufrancis@bitbucket.org/djiomoufrancis/idel-extranet-service.git

#Entrer dans le répertoire du fichier cloné
$ cd idel-extranet-service
```


## Processus de build
   - Faire le build du projet Angular (ex: ng build --base-href /idel-v2/extranet/ --prod) afin de générer le dossier dist.
   - vider le contenu du dossier "src/main/resources/views/extranet/"
   - Copier le contenu du  dossier dist (résultant de l'étape 1) dans le dossier "src/main/resources/views/extranet/" du projet idel-extranet-service
   - Générer l'archive a deployer
``` bash
#Entrer dans le répertoire du projet
$ cd idel-extranet-service

#Faire le Build
$ mvn clean package
```


## Le Livrable
   - une fois le build effectué avec succes, l'archive a deployer se trouve dans "target/idel-extranet-web.jar"


## Le Déploiement
Il suffit d'exécuter l'archive idel-extranet-web.jar pour démarrer un serveur et déployer l'application Front-End.
Néanmoins, un service systeme (idel-extranet.service) a été créé sur le serveur (DEV) dans le but de démarrer ce serveur de facon automatique par l'OS comme un background process.
Le service démarre l'archive se trouvant dans /home/fdjiomou/idel/deploy/idel-extranet-web.jar. Pour déployer :
   - Copier et remplacer l'archive générée dans "/home/fdjiomou/idel/deploy/"
   - Redémarrer le service idel-extranet
``` bash
$ sudo systemctl restart idel-extranet
```


## L'exécution
Lancer l'application idel-extranet via l'url: https://dev-www.banq.qc.ca/idel-v2/extranet/
